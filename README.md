# alpine-autopatch

This is a simple ash script for the automatic installation of patches on Alpine Linux.

Please note, that the automatic installation of patches is always a risky procedure, but at some point its simply
necessary. Always make sure, that you are making regularly and often backups.

When the script runs, it will install all patches that are available via the apk package manager. Further, it patches
your [mailcow-dockerized](https://github.com/mailcow/mailcow-dockerized) installation, if there is one on your system. 
If the script shouldn't patch your Mailcow, you should call it like `/opt/scripts/alpine-autopatch/patch.sh nomoo`.

The script can be used by adding it to the crontab of the root user. Example:

```
### run alpine-autopatch ###
45      4       *       *       *       /opt/scripts/alpine-autopatch/patch.sh
```

Further, here is a best practice snippet for the basic installation of the script:

```
server:~# mkdir /opt/scripts/
server:~# cd /opt/scripts/
server:/opt/scripts# git clone https://codeberg.org/wh0ami/alpine-autopatch.git
server:/opt/scripts# chown -R root:root alpine-autopatch/
server:/opt/scripts# chmod -R o-rwx alpine-autopatch/
server:/opt/scripts# chmod 0750 alpine-autopatch/ alpine-autopatch/patch.sh
```

Logs will be stored in the sub folder `logs/`. The script is using zstd for compressing log files. If zstd is not
installed, it will fall back to gzip. Logs that are older than 180 days will be deleted.

If the kernel version changed, the script will trigger an reboot as the last step.
