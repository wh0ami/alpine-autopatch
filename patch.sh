#!/bin/ash

### Logging configuration ###
LOGDIR="$(dirname $(readlink -f "$0"))/logs"
LOGFILE="${LOGDIR}/patchlog_$(date +'%Y-%m-%d_%H-%M-%S').log"
exec >>"$LOGFILE"
exec 2>&1
set -x

### Read proxy configuration ###
if [ -f /etc/profile.d/proxy.sh ]; then
  . /etc/profile.d/proxy.sh
fi

### Patchscript ###
echo "Refreshing local package cache..."
apk update

echo "Performing a dry run..."
apk upgrade -s

echo "Installing updates..."
apk upgrade

echo "Searching and displaying new config files..."
find / -iname "*.apk-new"

if [ "$1" != "nomoo" ] && [ -d /opt/mailcow-dockerized/ ]; then
  echo "Patching mailcow-dockerized..."
  cd /opt/mailcow-dockerized/ || exit 1
  # if update.sh returns an exitcode that is not zero, it will be executed again, since this can mean, that the update.sh file has updated itself and needs to be executed again, to apply the changes
  ./update.sh -f
  [ $? -ne 0 ] && ./update.sh -f
fi

### Cleaning up old logs ###
echo "Deleting all logs older than 180 days..."
find "$LOGDIR" -type f \( -iname "*.log" -iname "*.log.gz" -iname "*.log.zst" \) -mtime +180 -exec rm -vf {} \;

### Compressing and cleaning up logs ###
set +x
if [ -n "$(which zstd)" ]; then
  zstd -z -19 --threads "$(nproc)" --rm "$LOGFILE"
else
  gzip -9 "$LOGFILE"
fi

### Automatic reboot on kernel update ###
if [ "$(basename /lib/modules/*)" != "$(uname -r)" ]; then
  reboot
fi
